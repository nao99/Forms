<?php

namespace AppBundle\Admin\Inform;

use AppBundle\Model\ProductListQuery;
use AppBundle\Model\ProductListRel;
use AppBundle\Model\ProductListRelQuery;
use AppBundle\Model\ProductQuery;
use Creonit\AdminBundle\Component\Request\ComponentRequest;
use Creonit\AdminBundle\Component\Response\ComponentResponse;
use Creonit\AdminBundle\Component\Scope\Scope;
use Creonit\AdminBundle\Component\TableComponent;
use Symfony\Component\HttpFoundation\ParameterBag;

class ChoosePhotoAlbumTable extends TableComponent
{

    protected $activeProducts;

    /**
     * @title Выберите альбом
     * @action choose(options){
     *      var $row = this.findRowById(options.rowId);
     *      $row.toggleClass('success')
     *
     *      this.request('choose', $.extend({key: options.key}, this.getQuery()), {state: $row.hasClass('success')});
     *      this.parent.loadData();
     * }
     * @cols Название
     *
     * \PhotoAlbum
     *
     * @field title
     *
     * @col
     * {{ title | action('external', _key, title) | icon('paperclip') | controls }}
     *
     */
    public function schema()
    {


    }
}