<?php
/**
 * Created by PhpStorm.
 * User: makarov
 * Date: 04.07.2016
 * Time: 21:32
 */

namespace AppBundle\Admin\Inform;


//use AppBundle\Model\CacheQuery;
use AppBundle\Model\Inform;
/*use AppBundle\Util\Formatter;
use Propel\Runtime\Util\PropelModelPager;*/
use Creonit\AdminBundle\Component\EditorComponent;
use Creonit\AdminBundle\Component\Request\ComponentRequest;
use Creonit\AdminBundle\Component\Response\ComponentResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class InformEditor extends EditorComponent
{

	/**
	 * @entity Inform
	 * @title Новость
	 * @field photo_album_id:external {title: 'entity.getPhotoAlbum().getTitle()'}
	 * @field title {constraints: [NotBlank()]}
	 * @field created_at {load: 'entity.getCreatedAt("d.m.Y")'}
     * @field full_url
	 *
	 * @template
	 *
	 * {{ title | text | group('Заголовок') }}
	 * {{ slug | text({placeholder: 'Заполнится из названия' }) | group('Ссылка') }}
     * {% if _key %}
     *      {{ full_url | panel | group('Ссылка на предросмотр') }}
     * {% endif %}
	 * {{ description | textarea | group('Краткое содержание') }}
	 * {{ content | textedit | group('Содержание') }}
	 * {{ image_id | image | group('Изображение') }}
	 * {{ created_at | input('date', {placeholder: 'Заполнится автоматически из текущей даты'}) | group('Дата публикации') }}
     * {{ price | input('price', {placeholder: 'По умолчанию 0'}) | group('Цена') }}
	 *
	 * 	 <br>
	 *      {{ (
	 *          (((meta_title | text | group('Заголовок') | col(6)) ~ (meta_keywords | text | group('Ключи') | col(6))) | row) ~
	 *          (meta_description | textarea | group('Описание'))
	 *         ) | panel('default', 'Мета информация')
	 *      }}
	 *
	 */
	public function schema()
	{
	}

	/**
	 * @param ComponentRequest $request
	 * @param ComponentResponse $response
	 * @param Inform $entity
	 */
	public function preSave(ComponentRequest $request, ComponentResponse $response, $entity)
	{

		if($request->data->get('created_at')){
			$entity
				->setCreatedAt($request->data->get('created_at') ? date('Y-m-d H:i:s', strtotime($request->data->get('created_at'))) : '0000-00-00 00:00:00');
		}

		/*if(!$request->data->get('slug')){
			$entity->setSlug(Formatter::transliterate($request->data->get('title')));
		}

		foreach (CacheQuery::create()->findByCode('inform') as $item){
            $this->container->get('app.cache')->delete($item->getKey());
        }*/

	}

    /**
     * @param ComponentRequest $request
     * @param ComponentResponse $response
     * @param Inform $entity
     */
    public function postSave(ComponentRequest $request, ComponentResponse $response, $entity) {
        /*foreach (CacheQuery::create()->findByCode('inform') as $item){
            $this->container->get('app.cache')->delete($item->getKey());
        }*/
    }

    /*public function decorate(ComponentRequest $request, ComponentResponse $response, $entity)
    {
        if ($entity->getSlug()) {
            if($request->data->get('is_eng')){
                $url = $this->container->get('router')->generate('en_news.view', ['slug' => $entity->getSlug()], UrlGeneratorInterface::ABSOLUTE_URL);
            } elseif($request->data->get('type') == 1 || $entity->getType() == 1) {
                $url = $this->container->get('router')->generate('cabinet_news_item', ['slug' => $entity->getSlug()], UrlGeneratorInterface::ABSOLUTE_URL);
            } else{
                $url = $this->container->get('router')->generate('news.view', ['slug' => $entity->getSlug()], UrlGeneratorInterface::ABSOLUTE_URL);
            }

            $link = '<a href="'.$url.'" target="_blank">'.$url.'</a>';
            if($entity->isNew()) $link = 'Будет доступна после сохранения';
            $response->data->set('full_url', $link);
        }
    }*/

}