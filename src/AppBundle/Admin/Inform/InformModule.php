<?php

/**
 * Created by PhpStorm.
 * User: makarov
 * Date: 04.07.2016
 * Time: 21:22
 */

namespace AppBundle\Admin\Inform;

use Creonit\AdminBundle\Module;

class InformModule extends Module
{

	protected function configure()
	{
		$this
			->setTitle('Новости')
			->setIcon('newspaper-o')
			->setTemplate('InformTable')
			//->setPermission('ROLE_ADMIN_INFORM')
		;
	}

	public function initialize()
	{
		$this->addComponent(new InformTable);
		$this->addComponent(new InformEditor);
		$this->addComponent(new ChoosePhotoAlbumTable);
		$this->addComponent(new ChooseInformTable);
	}


}