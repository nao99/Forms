<?php

namespace AppBundle\Admin\Inform;

use AppBundle\Model\ProductListQuery;
use AppBundle\Model\ProductListRel;
use AppBundle\Model\ProductListRelQuery;
use AppBundle\Model\ProductQuery;
use Creonit\AdminBundle\Component\Request\ComponentRequest;
use Creonit\AdminBundle\Component\Response\ComponentResponse;
use Creonit\AdminBundle\Component\Scope\Scope;
use Creonit\AdminBundle\Component\TableComponent;
use Propel\Runtime\ActiveQuery\Criteria;
use Symfony\Component\HttpFoundation\ParameterBag;

class ChooseInformTable extends TableComponent
{

    protected $activeProducts;

    /**
     * @title Выберите новость
     * @action choose(options){
     *      var $row = this.findRowById(options.rowId);
     *      $row.toggleClass('success')
     *
     *      this.request('choose', $.extend({key: options.key}, this.getQuery()), {state: $row.hasClass('success')});
     *      this.parent.loadData();
     * }
     * @cols Название, Дата
     *
     * \Inform
     *
     * @field title
     * @field created {load: 'entity.getCreatedAt("d.m.Y")'}
     * @pagination 50
     *
     * @col {{ title | action('external', _key, title) | icon('paperclip') | controls }}
     * @col {{ created }}
     *
     */
    public function schema()
    {


    }

    protected function filter(ComponentRequest $request, ComponentResponse $response, $query, Scope $scope, $relation, $relationValue, $level)
    {
        $query->orderByCreatedAt(Criteria::DESC)->orderById(Criteria::DESC);
        $query->filterBySubsiteId(null);
    }


}