<?php
/**
 * Created by PhpStorm.
 * User: makarov
 * Date: 07.07.2016
 * Time: 12:08
 */

namespace AppBundle\Admin\Inform;


use AppBundle\Model\BlogQuery;
use AppBundle\Model\Inform;
use AppBundle\Model\InformQuery;
use Creonit\AdminBundle\Component\Request\ComponentRequest;
use Creonit\AdminBundle\Component\Response\ComponentResponse;
use Creonit\AdminBundle\Component\Scope\Scope;
use Creonit\AdminBundle\Component\TableComponent;
use Propel\Runtime\ActiveQuery\Criteria;

class InformTable extends TableComponent
{
    /**
     * @title Новости
     * @field is_eng:select
     * @header
     * <form class="form-inline pull-right">
     * {{ is_eng | select | group('Язык') }}
     * {{ search | text({placeholder: 'Поиск', size: 'sm'}) | group('Поиск') }}
     * {{ submit('Обновить', {size: 'sm'}) }}
     * </form>
     * {{ button('Добавить запись', {type: 'success', icon: 'newspaper-o', size: 'sm'}) | open('Inform.InformEditor') }}
     * @action copy(options){
     *      var $row = this.findRowById(options.rowId);
     *      this.request('copy', $.extend({inform_id: options.key}, this.getQuery()), {state: $row.hasClass('success')});
     *      this.loadData();
     * }
     *
     * @cols Запись, URL, Вид отображения, Дата,  .
     *
     * \Inform
     * @field title
     * @field slug
     * @field created {load: 'entity.getCreatedAt("d.m.Y")'}
     * @pagination 50
     *
     * @col {{ title | open('Inform.InformEditor', {key: _key}) | controls(   buttons(  button('', {icon: 'clone', size: 'xs'}) | action('copy', {key: _key, rowId: _row_id}) )  ) }}
     * @col {{ slug }}
     * @col {{ created }}
     * @col {{ buttons(_visible()~_delete()) }}
     *
     */

    public function schema()
    {
        $languages = [
            'ALL',
            'ENG',
        ];
        $this->getField('is_eng')->parameters->set('options', $languages);

        $this->setHandler('copy', function (ComponentRequest $request, ComponentResponse $response) {
            $inform = InformQuery::create()->findPk($request->query->get('inform_id')) or $response->flushError('Новость не найдена');
            $unicalCounter = InformQuery::create()->filterBySlug("%" . $inform->getSlug() . "%", Criteria::LIKE)->count();

            $copiedInform = $inform->copy(true);
            $copiedInform
                ->setTitle($inform->getTitle() .' (Копия)')
                ->setSlug($inform->getSlug() . ($unicalCounter == 1 ? '_copy' : '_copy' . $unicalCounter))
                ->setVisible(false)
                ->save()
            ;
        });
    }

    /**
     * @param ComponentRequest $request
     * @param ComponentResponse $response
     * @param InformQuery $query
     * @param Scope $scope
     * @param $relation
     * @param $relationValue
     * @param $level
     */
    protected function filter(ComponentRequest $request, ComponentResponse $response, $query, Scope $scope, $relation, $relationValue, $level)
    {
        $query->orderByCreatedAt(Criteria::DESC)->orderById(Criteria::DESC);
        ///$query->filterBySubsiteId(null);

        if($search = $request->query->get('search')){
            $query
                ->condition('state1', "Inform.Title LIKE ?", "%{$search}%")
                ->where(['state1'])
            ;
        }
        if ($request->query->get('is_eng')) {
            $query->filterByIsEng($request->query->get('is_eng'));
        } else {
            $query->clearOrderByColumns()->orderByCreatedAt(Criteria::DESC)->orderById(Criteria::DESC);
        }
    }
}