<?php

namespace AppBundle\Admin\News;

use Creonit\AdminBundle\Module;

class InformModule extends Module
{
    protected function configure()
    {
        $this->setTitle('Новости');
        $this->setTemplate('NewsTable');
        $this->setIcon('news');
        $this->setPermission('ROLE_NEWS');
    }


    public function initialize()
    {
        $this->addComponent(new NewsTable);
        $this->addComponent(new NewsEditor);
        $this->addComponent(new ChooseProjectListTable);
        $this->addComponent(new ReportTable);
        $this->addComponent(new ReportEditor);
    }
}