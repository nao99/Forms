<?php

namespace AppBundle\Admin\Order;

use Creonit\AdminBundle\Module;

class OrderModule extends Module
{

    protected function configure()
    {
        $this
            ->setTitle('Заказы')
            ->setIcon('credit-card')
            ->setTemplate('OrderTable')
            //->setPermission('ROLE_ADMIN_ORDER')
        ;
    }

    public function initialize()
    {
        $this->addComponent(new OrderTable);
        $this->addComponent(new OrderEditor);
        $this->addComponent(new OrderItemTable);
        $this->addComponent(new OrderItemEditor);
        $this->addComponent(new ChooseOrderTable);
        $this->addComponent(new ChooseOrderEditor);
    }


}