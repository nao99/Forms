<?php

namespace AppBundle\Admin\Order;

use AppBundle\Model\Order;
use Creonit\AdminBundle\Component\EditorComponent;
use Creonit\AdminBundle\Component\Request\ComponentRequest;
use Creonit\AdminBundle\Component\Response\ComponentResponse;
use Creonit\AdminBundle\Component\TableComponent;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ChooseOrderEditor extends EditorComponent
{
    /**
     * @title Заказанные товары
     * @header
     *
     * @action zero(key, rowId){
     * var $row = this.findRowById(rowId),
     *     $button = $row.find("td:eq(2)").find('.fa-check, .fa-ban'),
     *     zero = !$button.hasClass('fa-check');
     *
     * $button.toggleClass('fa-check', zero);
     * $button.toggleClass('fa-ban', !zero);
     *
     * this.request('zero', $.extend(this.getQuery(), {key: key}), {zero: zero}, function(response) {
     *   if(this.checkResponse(response)){
     *     $button.toggleClass('fa-check', zero);
     *     $button.toggleClass('fa-ban', !zero);
     *   }
     * });
     *}
     *
     *
     *
     * @cols ФИО, Статус, Общая цена, ID, .
     *
     * \Order
     *
     * @field status_id
     * @field total
     *
     * @col {{ fio | open('Order.OrderItemEditor', {key: _key}) | controls }}
     * @col {{ status_id }}
     * @col {{ total }}
     * @col {{ id }}
     * @col {{ '' | icon(in_zero ? 'check' : 'ban') | action('zero', _key, _row_id) }}
     *
     *
     */
    public function schema()
    {

    }

}