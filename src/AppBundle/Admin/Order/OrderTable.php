<?php

namespace AppBundle\Admin\Order;

use AppBundle\Model\OrderQuery;
use Creonit\AdminBundle\Component\Request\ComponentRequest;
use Creonit\AdminBundle\Component\Response\ComponentResponse;
use Creonit\AdminBundle\Component\Scope\Scope;
use Creonit\AdminBundle\Component\TableComponent;
use Propel\Runtime\ActiveQuery\Criteria;

class OrderTable extends TableComponent
{
    /**
     * @title Заказы
     * @header
     *
     * @cols ФИО заказчика, Сумма заказа, Дата заказа, Статус заказа
     *
     * \Order
     * @field fio
     * @field total
     * @field created {load: 'entity.getCreatedAt("d.m.Y")'}
     * @field status_id
     * @pagination 50
     *
     * @col {{ fio | open('Order.OrderEditor', {key: _key}) }}
     * @col {{ total }}
     * @col {{ created }}
     * @col {{ status_id }}
     *
     */

    public function schema()
    {
        /*$status = $this->getField('delete');
        if($status == false)
        {
            $status->parameters->set('asdf', '132');
        }*/
    }

    /**
     * @param ComponentRequest $request
     * @param ComponentResponse $response
     * @param OrderQuery $query
     * @param Scope $scope
     * @param $relation
     * @param $relationValue
     * @param $level
     */
    protected function filter(ComponentRequest $request, ComponentResponse $response, $query, Scope $scope, $relation, $relationValue, $level)
    {
        $query->orderByCreatedAt(Criteria::DESC)->orderById(Criteria::DESC);

        if($userId = $request->query->get('user_id'))
        {
            $id = $request->query->get('id');
            $query->filterByUserId($userId)->filterById($id, CRITERIA::NOT_EQUAL);
        }
    }


}