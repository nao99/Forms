<?php

namespace AppBundle\Admin\Order;

use AppBundle\Model\Order;
use Creonit\AdminBundle\Component\EditorComponent;
use Creonit\AdminBundle\Component\Request\ComponentRequest;
use Creonit\AdminBundle\Component\Response\ComponentResponse;
use Creonit\AdminBundle\Component\TableComponent;
use AppBundle\Model\OrderQuery;
use Creonit\AdminBundle\Component\Scope\Scope;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ChooseOrderTable extends TableComponent
{
    /**
     * @title Заказанные товары
     * @header
     *
     * @cols ФИО, Статус, Общая цена, ID заказа, ID пользователя
     *
     * \Order
     *
     * @field user_id
     *
     * @col {{ fio | open('Order.ChooseOrderEditor', {key: _key}) | controls }}
     * @col {{ status_id }}
     * @col {{ total }}
     * @col {{ id }}
     *
     *
     */
    public function schema()
    {

    }

    /**
     * @param ComponentRequest $request
     * @param ComponentResponse $response
     * @param OrderQuery $query
     * @param Scope $scope
     * @param $relation
     * @param $relationValue
     * @param $level
     */
    protected function filter(ComponentRequest $request, ComponentResponse $response, $query, Scope $scope, $relation, $relationValue, $level)
    {
        $query->filterByUserId($request->query->get('user_id'));
    }

}