<?php

namespace AppBundle\Admin\Order;

use AppBundle\Model\Order;
use Creonit\AdminBundle\Component\EditorComponent;
use Creonit\AdminBundle\Component\Request\ComponentRequest;
use Creonit\AdminBundle\Component\Response\ComponentResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class OrderEditor extends EditorComponent
{
    /**
     * @entity Order
     * @title Заказ
     * @field fio {constraints: [NotBlank()]}
     * @field created_at {load: 'entity.getCreatedAt("d.m.Y")'}
     * @field delivery {load: 'entity.getOrderDeliveryRelatedByDeliveryId().getDelivery()'}
     * @field user_id
     * @field id
     *
     * @template
     *
     * {{ fio | text | group('ФИО заказчика') }}
     * {{ email | text | group('E-mail заказчика') }}
     * {{ phone | text | group('Номер телефона заказчика') }}
     * {{ locality | text | group('Место назначения заказа') }}
     * {{ delivery | text | group('Доставка') }}
     * {{ delivery_price | text | group('Цена доставки') }}
     * {{ created_at | input('date', {placeholder: 'Заполнится автоматически из текущей даты'}) | group('Дата заказа') }}
     * {{ total | input('total', {placeholder: 'По умолчанию 0'}) | group('Сумма заказа') }}
     *
     * {% if _key %}
     *      {{ component('Order.OrderItemTable', {order_id: _key}) | group('Товары') }}
     * {% endif %}
     *
     * {% if _key %}
     *      {{ component('Order.OrderTable', {user_id: user_id, id: id}) | group('Другие заказы этого пользователя') }}
     * {% endif %}
     *
     */
    public function schema()
    {
        //$this->getField('category')->parameters->set('options', array_merge([0 => '',], Shop::getCategoryDb()));
    }

}