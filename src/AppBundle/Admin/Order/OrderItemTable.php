<?php

namespace AppBundle\Admin\Order;

use AppBundle\Model\Order;
use AppBundle\Model\OrderItem;
use AppBundle\Model\OrderItemQuery;
use AppBundle\Model\OrderQuery;
use Creonit\AdminBundle\Component\Request\ComponentRequest;
use Creonit\AdminBundle\Component\Response\ComponentResponse;
use Creonit\AdminBundle\Component\Scope\Scope;
use Creonit\AdminBundle\Component\TableComponent;

class OrderItemTable extends TableComponent
{
    /**
     * @title Заказанные товары
     * @header
     *
     * @action zero(key, rowId){
     * var $row = this.findRowById(rowId),
     *     $button = $row.find("td:eq(2)").find('.fa-check, .fa-ban'),
     *     zero = !$button.hasClass('fa-check');
     *
     * $button.toggleClass('fa-check', zero);
     * $button.toggleClass('fa-ban', !zero);
     *
     * this.request('zero', $.extend(this.getQuery(), {key: key}), {zero: zero}, function(response) {
     *   if(this.checkResponse(response)){
     *     $button.toggleClass('fa-check', zero);
     *     $button.toggleClass('fa-ban', !zero);
     *   }
     * });
     *}
     *
     *
     *
     * @cols Название, Количество, Цена за 1, ID, .
     *
     * \OrderItem
     *
     * @field title {load: 'entity.getProduct().getTitle()'}
     * @field in_zero
     * @field price
     * @field quantity
     *
     * @col {{ title | open('Order.OrderItemEditor', {key: _key}) | controls }}
     * @col {{ quantity }}
     * @col {{ price }}
     * @col {{ id }}
     * @col {{ '' | icon(in_zero ? 'check' : 'ban') | action('zero', _key, _row_id) }}
     *
     *
     */
    public function schema()
    {
        $this->addHandler('zero', function (ComponentRequest $request, ComponentResponse $response){
        $query = $request->query;
        if($request->data->has('zero') and $query->get('key'))
        {
            if($entity = OrderItemQuery::create()->findPk($query->get('key')))
            {
                $price = $entity->getPrice();
                $quantity = $entity->getQuantity();

                $order = OrderQuery::create()->findPk($request->query->get('order_id'));

                $oldAmount = $order->getAmount();
                $newAmount = $oldAmount - $price * $quantity;

                $oldTotal = $order->getTotal();
                $newTotal = $oldTotal - $price * $quantity;

                $order->setAmount($newAmount);
                $order->setTotal($newTotal);
                $order->save();

                if($newTotal == $order->getDeliveryPrice())
                {
                    $order->setStatusId(4);
                }

                $entity->setInZero($request->data->getBoolean('zero'));
                $entity->setQuantity(0);
                $entity->save();

                $response->data->set('success', true);
                $response->data->set('zero', $entity->getInZero());
            }
            else
            {
                $response->flushError('Элемент не найден');
            }
        }
        else
        {
            $response->flushError('Ошибка при выполнения запроса');
        }
        });
    }

    /**
     * @param ComponentRequest $request
     * @param ComponentResponse $response
     * @param OrderItemQuery $query
     * @param Scope $scope
     * @param $relation
     * @param $relationValue
     * @param $level
     */
    protected function filter(ComponentRequest $request, ComponentResponse $response, $query, Scope $scope, $relation, $relationValue, $level)
    {
        $query->filterByOrderId($request->query->get('order_id'));
    }
}