<?php

namespace AppBundle\Admin\Order;


use AppBundle\Model\Order;
use Creonit\AdminBundle\Component\EditorComponent;
use Creonit\AdminBundle\Component\Request\ComponentRequest;
use Creonit\AdminBundle\Component\Response\ComponentResponse;

class OrderItemEditor extends EditorComponent
{
    /**
     * @entity OrderItem
     * @title Товар
     *
     * @field title {load: 'entity.getProduct().getTitle()'}
     * @field type:select {constraints: [NotBlank()]}
     * @field validation_type:select
     *
     * @template
     *
     * {{ title | text | group('Название') }}
     * {{ quantity | text | group('Количество') }}
     * {{ price | text | group('Цена за 1') }}
     * {{ id | text | group('Идентификатор') }}
     *
     *
     */
    public function schema()
    {
    }

}