<?php

namespace AppBundle\Admin\Product;

use Creonit\AdminBundle\Module;

class ProductModule extends Module
{

    protected function configure()
    {
        $this
            ->setTitle('Товары и услуги')
            ->setIcon('shopping-cart')
            ->setTemplate('ProductTable')
            //->setPermission('ROLE_ADMIN_INFORM')
        ;
    }

    public function initialize()
    {
        $this->addComponent(new ProductTable);
        $this->addComponent(new ProductEditor);
        //$this->addComponent(new ChoosePhotoAlbumTable);
        //$this->addComponent(new ChooseInformTable);
    }


}