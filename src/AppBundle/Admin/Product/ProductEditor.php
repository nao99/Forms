<?php

namespace AppBundle\Admin\Product;

use AppBundle\Model\Product;
use Creonit\AdminBundle\Component\EditorComponent;
use Creonit\AdminBundle\Component\Request\ComponentRequest;
use Creonit\AdminBundle\Component\Response\ComponentResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ProductEditor extends EditorComponent
{

    /**
     * @entity Product
     * @title Товары и услуги
     * @field title {constraints: [NotBlank()]}
     * @field created_at {load: 'entity.getCreatedAt("d.m.Y")'}
     *
     * @template
     *
     * {{ title | text | group('Название товара') }}
     * {{ slug | text({placeholder: 'Заполнится из названия' }) | group('Ссылка на товар') }}
     * {{ description | textarea | group('Описание товара') }}
     * {{ image_id | image | group('Изображение') }}
     * {{ created_at | input('date', {placeholder: 'Заполнится автоматически из текущей даты'}) | group('Дата публикации') }}
     * {{ price | input('price', {placeholder: 'По умолчанию 0'}) | group('Цена') }}
     */
    public function schema()
    {
        //$this->getField('category')->parameters->set('options', array_merge([0 => '',], Shop::getCategoryDb()));
    }

}