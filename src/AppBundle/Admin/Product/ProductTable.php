<?php

namespace AppBundle\Admin\Product;

use AppBundle\Model\Product;
use AppBundle\Model\ProductQuery;
use Creonit\AdminBundle\Component\Request\ComponentRequest;
use Creonit\AdminBundle\Component\Response\ComponentResponse;
use Creonit\AdminBundle\Component\Scope\Scope;
use Creonit\AdminBundle\Component\TableComponent;
use Propel\Runtime\ActiveQuery\Criteria;

class ProductTable extends TableComponent
{
    /**
     * @title Товары и услуги
     * @header
     * {{ button('Добавить товар', {type: 'success', icon: 'shopping-cart', size: 'sm'}) | open('Product.ProductEditor') }}
     * @action copy(options){
     *      var $row = this.findRowById(options.rowId);
     *      this.request('copy', $.extend({product_id: options.key}, this.getQuery()), {state: $row.hasClass('success')});
     *      this.loadData();
     * }
     *
     * @cols Название товара, URL, Цена, Вид отображения,  .
     *
     * \Product
     * @field title
     * @field slug
     * @field price
     * @field created {load: 'entity.getCreatedAt("d.m.Y")'}
     * @pagination 50
     *
     * @col {{ title | open('Product.ProductEditor', {key: _key}) | controls(   buttons(  button('', {icon: 'clone', size: 'xs'}) | action('copy', {key: _key, rowId: _row_id}) )  ) }}
     * @col {{ slug }}
     * @col {{ price }}
     * @col {{ created }}
     * @col {{ buttons(_visible()~_delete()) }}
     *
     */

    public function schema()
    {

    }


}