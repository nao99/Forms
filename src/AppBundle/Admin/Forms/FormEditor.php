<?php

namespace AppBundle\Admin\Forms;


use Creonit\AdminBundle\Component\EditorComponent;

class FormEditor extends EditorComponent
{

	/**
	 * @entity Form
	 * @title Форма
	 *
	 * @field title {constraints: [NotBlank()]}
	 * @field code {constraints: [NotBlank()]}
	 * @field notification_email {constraints: [NotBlank()]}
	 * @field mailing_template_id:external {title: 'entity.getTitle()'}
     * @field role_id:external {title: 'entity.getUserRole().getTitle()', required: true}
	 *
	 * @template
	 *
	 * {{ title | text | group('Название') }}
	 * {{ title_form | text | group('Заголовок формы') }}
	 * {{ description | text | group('Подпись формы') }}
	 * {{ code | text | group('Идентификатор') }}
	 * {{ notification_email | text | group('Email для уведомления') }}
	 * {{ success_text | textedit | group('Сообщение пользователю при успешной отправке') }}
	 * {{ mailing_template_id | external('ChooseMailingTemplateTable', {empty: 'Шаблон по умолчанию'}) | group('Шаблон автоответа') }}
	 * {{ office_request | checkbox('Заявление в офисе') | group }}
	 *
	 * {% if _key %}
	 *      {{ component('Forms.FormFieldTable', {form_id: _key}) | group('Поля') }}
	 * {% else %}
	 *      {{ '<p>Сохраните форму, чтобы добавлять поля</p>' | raw | group('Поля') }}
	 * {% endif %}
	 *
	 */
	public function schema()
	{

	}
}