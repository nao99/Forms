<?php

namespace AppBundle\Admin\Forms;


use AppBundle\Model\FormQuery;
use AppBundle\Model\FormResult;
use AppBundle\Model\FormResultQuery;
use Creonit\AdminBundle\Component\Request\ComponentRequest;
use Creonit\AdminBundle\Component\Response\ComponentResponse;
use Creonit\AdminBundle\Component\Scope\Scope;
use Creonit\AdminBundle\Component\TableComponent;
use Propel\Runtime\ActiveQuery\Criteria;
use Symfony\Component\HttpFoundation\ParameterBag;

class FormResultTable extends TableComponent
{

    private $request;

	/**
     *
     * @action tocsv(request){
     *    this.request('tocsv', {request: request}, null, function(response){
     *      if (this.checkResponse(response)){
     *          console.log(window.location.host + response.data.url);
     *          window.location.href = ('//' + window.location.host + response.data.link);
     *      }
     *   });
     * }
     *
     * @title Результаты форм
     *
     * @field status:select
     * @field type_id:select
     * @field direction:select
     * @field editable
     *
	 * @header
     * {{ button('Экспорт списка', {size: 'sm', icon: 'download', type: 'info'}) | action('tocsv', _query) }}
     *  <form class="form-inline pull-right">
     * {{ type_id | select | group('Тип') }}
     * {{ direction | select | group('Направление карты') }}
     * {{ search | text({placeholder: 'Поиск', size: 'sm'}) }}
     * {{ submit('Обновить', {size: 'sm'}) }}
     * </form>
     *
     * {% if editable %}
	 *      {{ button('Формы', {icon: 'fa-list-alt', size: 'sm'}) | open('Forms.FormTable') }}
     * {% endif %}
     *
	 * @cols Форма, Имя, Телефон, Клуб, Статус, Отвечено, Время отправки
	 *
	 * \FormResult
	 * @field caption {load: 'entity.getCaption()'}
	 * @field created {load: 'entity.getCreatedAt("d.m.Y H:i")'}
	 * @field answered {load: 'entity.isAnswered()'}
	 * @field name {load: 'entity.getNameFull()'}
	 * @field club {load: 'entity.getClub()'}
	 * @field phone {load: 'entity.getPhone()'}
	 *
	 * @pagination 100
	 * @col {{ caption | open('Forms.FormResultEditor', {key: _key}) | controls }}
	 * @col {{ name }}
	 * @col {{ phone }}
	 * @col {{ club }}
	 * @col {{ status_caption }}
	 * @col {{ answered ? 'да' : 'нет' }}
	 * @col {{ created }}
	 *
	 */
	public function schema()
	{
        $formTypes[0] = 'Все типы';
        foreach(FormQuery::create()->filterByVisible(true)->find() as $formType) {
            $role = $formType->getUserRole();
            if ($role) {
                if ($this->container->get('security.authorization_checker')->isGranted('ROLE_' . strtoupper($role->getName()))){
                    $formTypes[$formType->getId()] = $formType->getTitle();
                }
            } else {
                $formTypes[$formType->getId()] = $formType->getTitle();
            }
        }

        $this->getField('type_id')->parameters->set('options', $formTypes);

        /*$formResult = FormResult::$statuses;
        $formResult[0] = 'Все статусы';
        $this->getField('status')->parameters->set('options', $formResult);*/

        $direction = [0 => 'Все', 1 => 'Спорт', 2 => 'Дети'];
        $this->getField('direction')->parameters->set('options', $direction);


        $this->setHandler('tocsv', function (ComponentRequest $request, ComponentResponse $response) {


            $rootDir = $this->container->get('kernel')->getRootDir();
            $csvFolder = $rootDir . '/../web/csvs/';
            if (!is_dir($csvFolder)) {
                mkdir($csvFolder);
            }
            $fileName = md5(microtime()) . '.csv';
            $filePath = $csvFolder . $fileName;
            $fl = fopen($filePath, 'w+');


            $formResult = FormResultQuery::create();
            $formResult = $this->filterQuery($formResult, $request);
            $formResult = $formResult->find();
            $data = [];
            $c = 0;
            foreach ($formResult as $form) {
                if (!$c) {
                    $data['Person'] = 'Имя';
                    $data['Club'] = 'Название клуба';
                    $data['Phone'] = 'Телефон';
                    $data['Form'] = 'Название формы';
                    $data['Status'] = 'Статус';
                    $data['IsAnswered'] = 'Отвечено';
                    $data['PushTime'] = 'Время отправки формы';
                    fputcsv($fl, $data);
                }
                $data['Person'] = $form->getNameFull() ?: 'Не известно';
                $data['Club'] = $form->getClub() ?: 'Не известно';
                $data['Phone'] = $form->getPhone() ?: 'Не известно';
                $data['Form'] = $form->getFormTitle() ?: 'Не известно';
                $data['Status'] = $form->getStatusCaption() ?: 'Не известно';
                $data['IsAnswered'] = $form->isAnswered() ? 'Да' : 'Нет';
                $data['PushTime'] = $form->getCreatedAt('Y-m-d H:i:s') ?: 'Не известно';
                fputcsv($fl, $data);
                $c++;
            }
            fclose($fl);
            $response->data->set('link', '/csvs/' . $fileName);
            $response->sendSuccess();
        });
	}

    /**
     * @param ComponentRequest $request
     * @param FormResultQuery $formResult
     */
	public function filterQuery($formResult, ComponentRequest $request) {

        $typeId = null;
        $statusId = null;
        $direction = null;
        $search = null;

        foreach ($request->query as $key => $value) {
            foreach ($value as $valueKey => $valueValue)
            {
                if ($valueKey == 'type_id')
                {
                    $typeId = $valueValue;
                }
                if ($valueKey == 'status')
                {
                    $statusId = $valueValue;
                }
                if ($valueKey == 'direction')
                {
                    $direction = $valueValue;
                }
                if ($valueKey == 'search')
                {
                    $search = $valueValue;
                }
            }
        }

        $formResult = $formResult->orderByCreatedAt(Criteria::DESC);

        if($search){
            $formResult = $formResult
                ->useFormResultFieldQuery()
                ->filterByValue("%$search%", Criteria::LIKE)
                ->endUse()
            ;
        }


        if($typeId){
            $formResult = $formResult
                ->filterByFormId($typeId)
            ;
        } else {
            foreach(FormQuery::create()->filterByVisible(true)->find() as $formType) {
                $role = $formType->getUserRole();
                if ($role) {
                    if (!$this->container->get('security.authorization_checker')->isGranted('ROLE_' . strtoupper($role->getName()))){
                        $formResult = $formResult
                            ->filterByFormId($formType->getId(), Criteria::NOT_EQUAL)
                        ;
                    }
                }
            }
        }

        if($statusId){
            $formResult = $formResult
                ->filterByStatus($statusId)
            ;
        }

        if($direction != 0){
            $formResult = $formResult
                ->useHolderQuery()
                ->useCardQuery()
                ->filterByChildrenCard($request->query->getInt('direction') == 2 ? true : false)
                ->endUse()
                ->endUse();
        }

        return $formResult;
    }

    /**
     * @param ComponentRequest $request
     * @param ComponentResponse $response
     * @param FormResultQuery $query
     * @param Scope $scope
     * @param $relation
     * @param $relationValue
     * @param $level
     */
	protected function filter(ComponentRequest $request, ComponentResponse $response, $query, Scope $scope, $relation, $relationValue, $level)
	{
        $this->request = $request;

		$query->orderByCreatedAt(Criteria::DESC);

        if($search = $request->query->get('search')){
            $query
                ->useFormResultFieldQuery()
                    ->filterByValue("%$search%", Criteria::LIKE)
                ->endUse()
            ;
        }

        if($typeId = $request->query->get('type_id')){
            $query
                ->filterByFormId($typeId)
            ;
        } else {
            foreach(FormQuery::create()->filterByVisible(true)->find() as $formType) {
                $role = $formType->getUserRole();
                if ($role) {
                    if (!$this->container->get('security.authorization_checker')->isGranted('ROLE_' . strtoupper($role->getName()))){
                        $query
                            ->filterByFormId($formType->getId(), Criteria::NOT_EQUAL)
                        ;
                    }
                }
            }
        }

        if($statusId = $request->query->get('status')){
            $query
                ->filterByStatus($statusId)
            ;
        }

        if($request->query->getInt('direction') != 0){
            $query
                ->useHolderQuery()
                ->useCardQuery()
                ->filterByChildrenCard($request->query->getInt('direction') == 2 ? true : false)
                ->endUse()
                ->endUse();
        }
	}

    protected function decorate(ComponentRequest $request, ComponentResponse $response, ParameterBag $data, $entity, Scope $scope, $relation, $relationValue, $level) {
        $isEditable = $this->container->get('security.authorization_checker')->isGranted('ROLE_ADMIN_FORMS_EDIT');
        $response->data->set('editable', $isEditable);
    }

}