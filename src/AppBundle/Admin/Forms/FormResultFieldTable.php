<?php

namespace AppBundle\Admin\Forms;


use AppBundle\Model\FormResultFieldQuery;
use AppBundle\Model\FormResultQuery;
use AppBundle\Model\OfficeConsultantQuery;
use Creonit\AdminBundle\Component\Request\ComponentRequest;
use Creonit\AdminBundle\Component\Response\ComponentResponse;
use Creonit\AdminBundle\Component\Scope\Scope;
use Creonit\AdminBundle\Component\TableComponent;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class FormResultFieldTable extends TableComponent
{

	/**
	 * @title Поля формы
	 * @header
	 * {% if _query.code == 'acess_lkk' %}
	 * 		{{ button('Отправить ссылку', {type: 'success', icon: 'key', size: 'sm'}) | action('sendPassword', _query.form_result_id) }}
	 * {% endif %}
	 *
	 * @action sendPassword(form_result_id) {
	 *      this.request('sendPassword', {result_id: form_result_id}, null, function(response){
	 *          if(this.checkResponse(response)) {
	 * 				if(response.data.error){
	 *					alert(response.data.error);
	 * 				}else{
	 *					alert('Ссылка на восстановление пароля отправлена');
	 * 				}
	 *
	 *          }
	 *      }.bind(this));
	 * }
	 * @cols Поле, Ответ
	 *
	 * \FormResultField
	 * @sortable true
	 * @field title {load: 'entity.getFieldTitle()'}
	 * @field answer {load: 'entity.getAnswerValue()'}
	 *
	 * @col {{ title }}
	 * @col {{ answer | raw }}
	 *
	 *
	 */
	public function schema()
	{
		$this->setHandler('sendPassword', [$this, 'sendPassword']);
	}

	public function sendPassword(ComponentRequest $request, ComponentResponse $response)
	{

		$result = FormResultQuery::create()->findPk($request->query->get('result_id'));
		$email = $result->getEmail();
		$contractId = $result->getContract();
		$consultant = OfficeConsultantQuery::create()->findPk($contractId);

		if($consultant){

			$user = $consultant->getUser();
			$user->setPasswordSecret($secret = $this->container->get('AuthorizationService')->generateSecret())->save();


			//$userService = $this->container->get('app.user_service');
			$mailing = $this->container->get('creonit_mailing');
			$mailing->setTemplate('AppBundle::mailCabinet.html.twig');

			$mailing->send(
				$mailing->createMessage('', ['auth.remind_password_cabinet' => [
					'user' => $user,
					'email' => $email,
					'contract' => $consultant->getContract(),
					'url' => $this->container->get('router')->generate('cabinet_password_reset.change', ['userId' => $user->getId(), 'secret' => $secret], UrlGeneratorInterface::ABSOLUTE_URL)
				]]),
				$email
			);

			$response->data->set('success', true);
		}else{
			$response->data->set('error', 'Консультант не найден');
		}

	}

	/**
	 * @param ComponentRequest $request
	 * @param ComponentResponse $response
	 * @param FormResultFieldQuery $query
	 * @param Scope $scope
	 * @param $relation
	 * @param $relationValue
	 * @param $level
	 */
	protected function filter(ComponentRequest $request, ComponentResponse $response, $query, Scope $scope, $relation, $relationValue, $level)
	{
		$query->filterByResultId($request->query->get('form_result_id'))->orderBySortableRank();
	}
}