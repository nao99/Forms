<?php

namespace AppBundle\Model;

use AppBundle\Model\Base\OrderStateQuery as BaseOrderStateQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'order_state' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class OrderStateQuery extends BaseOrderStateQuery
{

}
