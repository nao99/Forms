<?php

namespace AppBundle\Service;

use AppBundle\Model\ProductQuery;
use Symfony\Component\DependencyInjection\ContainerInterface;


class CartService
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function setCart($cart)
    {
        $this->container->get('session')->set('cart', $cart);
    }

    public function getCart()
    {
        return $this->container->get('session')->get('cart');
    }

    public function deleteCart()
    {
        $this->container->get('session')->set('cart', []);
    }

    public function getList()
    {
        $cart = $this->getCart();

        if($cart)
        {
            $productList = [];
            foreach($cart as $key => $value)
            {
                $product = ProductQuery::create()->filterByVisible(true)->findPk($key);
                if(!$product)
                {
                    continue;
                }
                $productList[$key]['product'] = $product;
                $productList[$key]['quantity'] = $value;
            }

            return $productList;
        }
        else
        {
            return [];
        }
    }

    public function getTotal()
    {
        $cart = $this->getCart();
        $cartList = $this->getList();

        $totalAmount = 0;
        $totalQuantity = 0;
        foreach($cartList as $key => $value)
        {
            $price = $value['product']->getPrice();
            $totalQuantity += $cart[$key];
            $quantity = $cart[$key];
            $totalAmount += $price * $quantity;
        }

        return ['amount' => $totalAmount, 'quantity' => $totalQuantity];
    }

    public function add($id)
    {
        $cart = $this->getCart();

        if($cart == null)
        {
            $cart[$id] = 1;
        }
        else
        {
            if(array_key_exists($id, $cart))
            {
                $value = $cart[$id];
                $cart[$id] = ++$value;
            }
            else
            {
                $cart[$id] = 1;
            }
        }

        $this->setCart($cart);
    }

    public function delete($id)
    {
        $cart = $this->getCart();

        unset($cart[$id]);

        $this->setCart($cart);
    }

    public function update($id, $value)
    {
        $cart = $this->getCart();
        $productValue = $cart[$id];

        if($value == 1)
        {
            $cart[$id] = ++$productValue;
        }
        else if($value == 0)
        {
            $cart[$id] = --$productValue;
        }

        $this->setCart($cart);
    }

    public function render()
    {
        $cartList = $this->getList();
        $total = $this->getTotal();
        $html = $this->container->get('twig')->render('cart_product.html.twig', ['cartList' => $cartList, 'total' => $total]);

        return $html;
    }
}