<?php

namespace AppBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;


class Date
{

    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getPrettyDate(\DateTime $date)
    {
        $months = [
            '1' => 'января',
            '2' => 'февраля',
            '3' => 'марта',
            '4' => 'апреля',
            '5' => 'мая',
            '6' => 'июня',
            '7' => 'июля',
            '8' => 'августа',
            '9' => 'сентября',
            '10' => 'октября',
            '11' => 'ноября',
            '12' => 'декабря',
        ];

        $dateTime = $date->format('j.n');
        $dateTime = explode('.', $dateTime);
        $today = $dateTime[0] . " " . $months[$dateTime[1]];

        return $today;
    }

}