<?php

namespace AppBundle\Service;

use AppBundle\Model\OrderItemQuery;
use AppBundle\Model\OrderQuery;
use AppBundle\Model\OrderStatusQuery;
use AppBundle\Model\ProductQuery;
use AppBundle\Model\OrderDeliveryQuery;
use AppBundle\Model\OrderCountryQuery;
use AppBundle\Model\OrderStateQuery;
use AppBundle\Model\Order;
use AppBundle\Model\OrderItem;
use Propel\Runtime\ActiveQuery\Criteria;
use Symfony\Component\DependencyInjection\ContainerInterface;

class OrderService
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function validate($data)
    {
        $errors = [];

        if(key_exists('fio', $data))
        {
            if($data['fio'])
            {
                $data['fio'] = trim($data['fio']);
            }
            else
            {
                $errors['fio'] = 'Введите ФИО';
            }
        }

        if(key_exists('locality', $data))
        {
            if(!$data['locality'])
            {
                $errors['locality'] = 'Введите населенный пункт';
            }
        }

        if(key_exists('email', $data))
        {
            if($data['email'])
            {
                $data['email'] = trim($data['email']);
                if(preg_match("/^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/", $data['email']) == 0)
                {
                    $errors['email'] = 'Введите корректный email';
                }
            }
            else
            {
                $errors['email'] = 'Введите email';
            }
        }

        if(key_exists('phone', $data))
        {
            if($data['phone'])
            {
                if(preg_match("/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/", $data['phone']) == 0)
                {
                    $errors['phone'] = 'Введите корректный номер телефона';
                }
            }
            else
            {
                $errors['phone'] = 'Введите номер телефона';
            }
        }

        if(key_exists('delivery', $data))
        {
            if($data['delivery']) //Проверка
            {
                $delivery = OrderDeliveryQuery::create()->findOneByDeliveryCode($data['delivery']);
                if(!$delivery)
                {
                    $errors['delivery'] = 'Такой доставки не существует';
                }
            }
            else
            {
                $errors['delivery'] = 'Выберите способ доставки';
            }
        }

        if(key_exists('country', $data))
        {
            if($data['country'])
            {
                $country = OrderCountryQuery::create()->findOneByCountryCode($data['country']);
                if(!$country)
                {
                    $errors['country'] = 'Такой страны не существует';
                }
                else
                {
                    $data['country'] = $country->getCountry();
                }
            }
            else
            {
                $errors['country'] = 'Выберите страну';
            }
        }

        return $errors;
    }

    public function getTotal($deliveryType = null)
    {
        $cart = $this->container->get('app.cart')->getCart();
        $cartList = $this->container->get('app.cart')->getList();

        $totalAmount = 0;
        $totalQuantity = 0;
        foreach($cartList as $key => $value)
        {
            $price = $value['product']->getPrice();
            $totalQuantity += $cart[$key];
            $quantity = $cart[$key];
            $totalAmount += $price * $quantity;
        }

        if($deliveryType)
        {
            $delivery = OrderDeliveryQuery::create()->findOneByDeliveryCode($deliveryType);
            $deliveryPrice = $delivery->getDeliveryPrice();
            $totalAmountAndDelivery = $totalAmount + $deliveryPrice;

            return ['amount' => $totalAmount, 'quantity' => $totalQuantity, 'deliveryPrice' => $deliveryPrice, 'total' => $totalAmountAndDelivery];
        }
        else
        {
            return ['amount' => $totalAmount, 'quantity' => $totalQuantity, 'deliveryPrice' => 'Выберите способ доставки', 'total' => 'Цена не рассчитается, пока Вы не выберете способ доставки'];
        }
    }

    public function saveOrder($data)
    {
        $total = $this->getTotal($data['delivery']);
        $cartList = $this->container->get('app.cart')->getList();
        $orderStatus = OrderStatusQuery::create()->findOneByStatusCode('new');
        $orderDelivery = OrderDeliveryQuery::create()->findOneByDeliveryCode($data['delivery']);
        $orderState = OrderStateQuery::create()->findOneByStateCode('notPaid');

        if($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            $user = $this->container->get('security.token_storage')->getToken()->getUser();
        }
        else
        {
            $user = null;
        }

        $order = new Order();
        $order
            ->setUser($user)
            ->setOrderStatus($orderStatus)
            ->setOrderDeliveryRelatedByDeliveryId($orderDelivery)
            ->setOrderDeliveryRelatedByDeliveryPrice($orderDelivery)
            ->setOrderState($orderState)
            ->setFio($data['fio'])
            ->setEmail($data['email'])
            ->setPhone($data['phone'])
            ->setLocality($data['locality'])
            ->setAmount($total['amount'])
            ->setTotal($total['total'])
        ;

        $order->save();

        foreach($cartList as $key => $value)
        {
            $orderItem = new OrderItem();

            $orderItem
                ->setOrder($order)
                ->setQuantity($value['quantity'])
                ->setProduct($value['product'])
                ->setPrice($value['product']->getPrice())
                ->setFio($data['fio'])
            ;

            $orderItem->save();
        }
    }

    public function render($template, $item, $params = null)
    {
        if($params)
        {
            if(key_exists('delivery', $params))
            {
                $params['delivery'] = OrderDeliveryQuery::create()->findOneByDeliveryCode($params['delivery'])->getDelivery();
            }
        }

        return $this->container->get('twig')->render($template, ['item' => $item, 'params' => $params]);
    }

    public function getOrderList($id = null, $orders = null)
    {
        if($id)
        {
            $orderList = OrderQuery::create()->findpk($id);
            if($orderList)
            {
                $orderList->getOrderItemsJoinProduct();
                $orderList->getOrderStatus();
                $orderList->getOrderDeliveryRelatedByDeliveryId();
            }
            else
            {
                $orderList = [];
            }

            return $orderList;
        }
        else
        {
            $orderList = [];
            if($orders)
            {
                foreach($orders as $order)
                {
                    $order->getOrderStatus();
                    $order->getOrderState();
                    $orderList[] = $order;
                }
            }
            else
            {
                $orderList = [];
            }

            return $orderList;
        }
    }
}