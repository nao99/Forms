<?php

namespace AppBundle\Service;

use AppBundle\Model\Form;
use AppBundle\Model\FormResult;
use AppBundle\Model\FormResultField;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Model\FormQuery;
use Symfony\Component\HttpFoundation\JsonResponse;


class FormService
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getForm($code)
    {
        $form = FormQuery::create()
            ->filterByVisible(true)
            ->findOneByCode($code)
        ;

        return $form;
    }

    public function getField($form)
    {
        $fields = $form
            ->getFields()
            ->getData()
        ;

        return $fields;
    }

    public function validate($data, $code)
    {
        if($data != null)
        {
            $form = $this->getForm($code);
            $fields =  $this->getField($form);

            $errors = [];
            foreach($fields as $field)
            {
                $nameField = 'field_' . $field->getId();

                switch($field->getValidationType())
                {
                    case Form::VALIDATION_ALPHABETICAL :
                        if( ($field->getRequired() == true) && ($data[$nameField] != null) )
                        {
                            $data[$nameField] = trim($data[$nameField]);
                        }
                        else if( ($field->getRequired() == true) && ($data[$nameField] == null) )
                        {
                            $errors[$nameField] = $field->getRequiredError();
                        }
                    break;

                    case Form::VALIDATION_DIGITS :
                        if( ($field->getRequired() == true) && ($data[$nameField] != null) )
                        {
                            if( preg_match('#^[0-9]+$#', $data[$nameField]) == 0 )
                            {
                                $errors[$nameField] = $field->getInvalidError();
                            }
                        }
                        else if( ($field->getRequired() == true) && ($data[$nameField] == null) )
                        {
                            $errors[$nameField] = $field->getRequiredError();
                        }
                    break;

                    case Form::VALIDATION_EMAIL :
                        if( ($field->getRequired() == true) && ($data[$nameField] != null) )
                        {
                            if( preg_match("/[^\s]+@[^\s\.]+\.[a-z]+/", $data[$nameField]) == 0 )
                            {
                                $errors[$nameField] = $field->getInvalidError();
                            }
                        }
                        else if( ($field->getRequired() == true) && ($data[$nameField] == null) )
                        {
                            $errors[$nameField] = $field->getRequiredError();
                        }
                    break;

                    case Form::VALIDATION_PHONE :
                        if( ($field->getRequired() == true) && ($data[$nameField] != null) )
                        {
                            if( preg_match("/^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/", $data[$nameField]) == 0 )
                            {
                                $errors[$nameField] = $field->getInvalidError();
                            }
                        }
                        else if( ($field->getRequired() == true) && ($data[$nameField] == null) )
                        {
                            $errors[$nameField] = $field->getRequiredError();
                        }
                    break;
                }
            }
            return $errors;
        }
    }

    public function render($code)
    {
        $form = $this->getForm($code);
        if($form == null)
        {
            echo "Error: Form not found";
        }
        else
        {
            $formData = $form->getFields()->getData();

            $html = "<form method='POST' action='/form/".$code."/' id=".'form_'.$form->getId()." class='form_for_load'>";

            foreach($formData as $field)
            {
                switch($field->getType())
                {
                    case Form::FIELD_TEXT :
                        $html .= "<label class='"."input'"." js-input>";

                        if($field->getValidationType() == Form::VALIDATION_PHONE)
                        {
                            $html .= "<input class='"."input__self'"."type='"."text' name='field_".$field->getId()."' data-masked=\"phone\">";
                        }
                        else
                        {
                            $html .= "<input class='"."input__self'"."type='"."text' name='field_".$field->getId()."'>";
                        }
                        $html .= "<span class='"."input__label'>".$field->getTitle()."</span>" . "</label>";
                        break;

                    case Form::FIELD_TEXTAREA :
                        $html .= "<label class='"."input input_textarea'"." js-input>";
                        $html .= "<textarea class='"."input__self'"."type='"."text' name='field_".$field->getId()."'></textarea>";
                        $html .= "<span class='"."input__label'>".$field->getTitle()."</span>" . "</label>";
                        break;
                }
            }

            $html .="<div class=\"form__actions\">";
            $html .= "<button class=\"button button_full button_big\" type=\"submit\"><span>Отправить</span></button>";
            $html .= "</div>"."</form>";

            return $html;
        }
    }

    public function saveResult($data, Form $form, $ip)
    {
       $result = new FormResult();
       $result
           ->setForm($form)
           ->setIpAddress($ip)
           ->save()
       ;

       //Теперь без тримов и нэкстов, но с твоими любимыми циклами :) Через foreach не получается, ибо в data у меня же только массив с ключем и значением field_'id' => 'значение поля'
       //Самого id там нет. Поэтому я не могу извлечь его в foreach

       $fieldId = $this->getField($form);
       $i = 0;
       while($i < count($fieldId))
       {
           $resultField = new FormResultField();
           $resultField->setForm($form);
           $resultField->setFormResult($result);
           $resultField->setFieldId($fieldId[$i]->getId());
           $resultField->setValue($data['field_'.$fieldId[$i]->getId()]);
           $resultField->save();

           $i++;
       }

       /*foreach($data as $data_field)
       {
           $FieldId = trim(key($data), 'field_');
           next($data);
           $resultField = new FormResultField();
           $resultField->setForm($form);
           $resultField->setFormResult($result);
           $resultField->setFieldId($FieldId);
           $resultField->setValue($data_field);
           $resultField->save();
       }*/
    }
}