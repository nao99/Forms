<?php

namespace AppBundle\Controller;

use AppBundle\Model\Form;
use AppBundle\Service\FormService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/*use AppBundle\Model\FormQuery;*/

class FormController extends Controller
{
    /**
     * @Route("/form/{code}/", name="form")
     */
    public function formAction(Request $request, $code)
    {
        $formService = $this->get('app.form');

        $form = $formService->getForm($code);

        if($form == null)
        {
            throw $this->createNotFoundException();
        }
        else
        {
            $fields = $formService->getField($form);

            foreach($fields as $field)
            {
                $data['field_' . $field->getId()] = $request->request->get('field_' . $field->getId());
            }

            $ip = $request->getClientIp();
            $errors = $formService->validate($data, $code);

            if(empty($errors))
            {
                $formService->saveResult($data, $form, $ip);

                return new JsonResponse([
                    'success' => true,
                    'success_text' => $form->getSuccessText()
                ]);
            }
            else
            {
                return new JsonResponse([
                    'success' => false,
                    'errors_text' => $errors
                ]);
            }
        }
    }
}
