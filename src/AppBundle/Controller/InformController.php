<?php

namespace AppBundle\Controller;

use AppBundle\Model\InformQuery;
/*use AppBundle\Model\InformPagination;*/
use Propel\Runtime\ActiveQuery\Criteria;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class InformController extends Controller
{
    /**
     * @Route("/news/", name="news_index")
     */
    public function indexAction(Request $request)
    {

        $page = $request->get('page') ?: 1;
        $per_page = 1;

        $pagination = InformQuery::create()
            ->filterByVisible(true)
            ->orderByCreatedAt(Criteria::DESC)
            ->paginate($page, $per_page)
        ;

        if($request->isXmlHttpRequest()){
            return $this->render('inform/list.html.twig', [
                'list' => $pagination->getResults(),
                'pages' => $pagination->getLinks(),
                'current_page' => $page
            ]);
        }


        return $this->render('inform/index.html.twig', [
            'list' => $pagination->getResults(),
            'pages' => $pagination->getLinks(),
            'current_page' => $page
        ]);
    }

    /**
     * @Route("/news/{slug}/", name="news_once")
     */
    public function onceAction(Request $request, $slug)
    {
        $inform = InformQuery::create()
            ->filterBySlug($slug)
            ->filterByVisible(true)
            ->findOne()
        ;

        if(!$inform){
            throw $this->createNotFoundException();
        }

        $this->get('creonit_page')
            ->getActivePage()
            ->setMetaTitle($inform->getTitle());

        return $this->render('inform/once_inform.html.twig', [
            'inform' => $inform
        ]);
    }
}
