<?php

namespace AppBundle\Controller;

use AppBundle\Model\ProductQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ProductController extends Controller
{
    /**
     * @Route("/product/", name="index")
     */
    public function indexAction(Request $request)
    {
        $product = ProductQuery::create()
            ->filterByVisible(true)
            ->orderByCreatedAt(Criteria::DESC)
            ->find()
        ;

        $this->get('creonit_page')->setMetaTitle('Товары и услуги');

        $cartList = $this->get('app.cart')->getList();
        $total = $this->get('app.cart')->getTotal();

        return $this->render('product.html.twig', [
            'product' => $product,
            'cartList' => $cartList,
            'total' => $total
        ]);
    }
}