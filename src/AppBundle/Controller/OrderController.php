<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class OrderController extends Controller
{
    /**
     * @Route("/order/", name="order")
     *
     */
    public function orderAction(Request $request)
    {
        $cartList = $this->get('app.cart')->getList();

        if($cartList)
        {
            if($request->isXmlHttpRequest())
            {
                $deliveryType = $request->request->get('deliveryType');
                $total = $this->get('app.order')->getTotal($deliveryType);

                $item = $cartList;
                $params = $total;

                return new JsonResponse([
                    'html' => $this->get('app.order')->render('cart_order.html.twig', $item, $params)
                ]);
            }
            else
            {
                $total = $this->get('app.order')->getTotal();

                $params = $total;
                $item = $cartList;

                return $this->render('order.html.twig', ['item' => $item, 'params' => $params]);
            }
        }
        else
        {
            return $this->render('empty_order.html.twig');
        }
    }

    /**
     * @Route("/order/confirm/", name="confirm")
     * @Method("POST")
     */
    public function confirmAction(Request $request)
    {
        $data = $request->request->all();

        if($data)
        {
            $orderService = $this->get('app.order');
            $errors = $orderService->validate($data);

            if(empty($errors))
            {
                $orderService->saveOrder($data);
                $total = $orderService->getTotal($data['delivery']);

                $cartList = $this->get('app.cart')->getList();
                $params = array_merge($total, $data);

                $html = $orderService->render('success_order.html.twig', $cartList, $params);
                $this->get('app.cart')->deleteCart();

                return new JsonResponse([
                    'status' => true,
                    'html' => $html
                ]);
            }
            else
            {
                return new JsonResponse([
                    'status' => false,
                    'text' => $errors
                ]);
            }
        }
    }
}