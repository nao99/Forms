<?php

namespace AppBundle\Controller;

use AppBundle\Model\ProductQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CartController extends Controller
{
    /**
     * @Route("/cart/", name="productAdd")
     * @Method("POST")
     *
     */
    public function productAddAction(Request $request)
    {
        $cartService = $this->get('app.cart');

        $id = $request->request->get('id');

        $checkId = ProductQuery::create()->filterByVisible(true)->findPk($id);

        if($checkId != null)
        {
            $cartService->add($id);

            return new JsonResponse([
                'success' => 'success',
                'html' => $cartService->render()
            ]);
        }
        else
        {
            throw $this->createNotFoundException();
        }
    }

    /**
     * @Route("/cart/", name="productUpdate")
     * @Method("PUT")
     *
     */
    public function productUpdateAction(Request $request)
    {
        $cartService = $this->get('app.cart');

        $id = $request->request->get('id');
        $value = $request->request->get('value');

        $checkId = ProductQuery::create()->filterByVisible(true)->findPk($id);

        if($checkId != null)
        {
            $cartService->update($id, $value);

            return new JsonResponse([
                'success' => 'success',
                'html' => $cartService->render()
            ]);
        }
        else
        {
            throw $this->createNotFoundException();
        }
    }

    /**
     * @Route("/cart/", name="productDelete")
     * @Method("DELETE")
     *
     */
    public function productDeleteAction(Request $request)
    {
        $cartService = $this->get('app.cart');

        $id = $request->request->get('id');

        $checkId = ProductQuery::create()->filterByVisible(true)->findPk($id);

        if($checkId != null)
        {
            $cartService->delete($id);

            return new JsonResponse([
                'success' => 'success',
                'html' => $cartService->render()
            ]);
        }
        else
        {
            throw $this->createNotFoundException();
        }
    }
}