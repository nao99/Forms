<?php

namespace AppBundle\Controller;

use AppBundle\Model\UserQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Email;

class AuthController extends Controller
{
    /**
     * Авторизация
     * @Route("/auth/", name="auth")
     */
    public function authAction(Request $request)
    {
        $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED');
        $user = $this->get('security.token_storage')->getToken()->getUser();

        return $this->render('auth.html.twig');
    }

    /**
     * Авторизация
     * @Route("/login/", name="login")
     * @Method("POST")
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function loginAction(Request $request)
    {
        $login = $request->request->get('login');
        $password = $request->request->get('password');
        $user = UserQuery::create()->filterByLogin($login)->findOne();

        if(!$user)
        {
            $response = [
                'error' => [
                    'request' => [
                        'login' => 'Неверный логин',
                    ],
                ]
            ];

            return new JsonResponse([
                'response' => $response
            ]);
        }

        $valid = $this->container->get('security.password_encoder')->isPasswordValid($user, $password);

        if($valid)
        {
            $this->container->get('app.authorization')->authorizeUser($user);

            return new JsonResponse([
                'response' => true
            ]);
        }
        else
        {
            $response = [
                'error' => [
                    'request' => [
                        'password' => 'Неверный пароль',
                    ],
                ]
            ];

            return new JsonResponse([
                'response' => $response
            ]);
        }
    }

    /**
     * Разовторизация
     * @Route("/logout/", name="logout")
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function logoutAction(Request $request)
    {
        return $this->redirectToRoute('index');
    }
}