<?php

namespace AppBundle\Controller;

use AppBundle\Model\OrderQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class OfficeController extends Controller
{
    /**
     * @Route("/office/", name="office")
     *
     */
    public function officeAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        return $this->render('order_status.html.twig', [
            'user' => $user
        ]);
    }

    /**
     * @Route("/office/orders/", name="orders")
     *
     */
    public function ordersAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $userId = $user->getId();

        $page = $request->get('page') ?: 1;
        $per_page = 3;

        $orders = OrderQuery::create()
            ->orderByCreatedAt(Criteria::DESC)
            ->filterByUserId($userId)
            ->paginate($page, $per_page)
        ;

        $orderList = $this->get('app.order')->getOrderList(null, $orders);

        if($request->isXmlHttpRequest()){
            return $this->render('order_table.html.twig', [
                'item' => $orderList,
                'pages' => $orders->getLinks(),
                'current_page' => $page
            ]);
        }

        return $this->render('order_status.html.twig', [
            'user' => $user,
            'item' => $orderList,
            'pages' => $orders->getLinks(),
            'current_page' => $page
        ]);
    }

    /**
     * @Route("/office/orders/{slug}/", name="alone")
     *
     */
    public function aloneAction(Request $request, $slug)
    {
        $id = $slug;

        $orderList = $this->get('app.order')->getOrderList($id);

        if($orderList)
        {
            return $this->render('one_order.html.twig', [
                'item' => $orderList
            ]);
        }
        else
        {
            throw $this->createNotFoundException();
        }
    }
}